# OpenLab Stifthalter

## Überblick

Praktischer Stifthalter für kleinteilige Büromaterialien

![](images/stifthalter-vorn.jpg){width=40%} ![](images/stifthalter-hinten.jpg){width=40%}

## Quelldateien

Das Modell wurde mit [Shapr3D](https://www.shapr3d.com/) erstellt.

Zur weiteren Bearbeitung also einfach [OpenLab-Stifthalter.shapr](OpenLab-Stifthalter.shapr) editieren und für den Druck dann bspw. als STL-Datei exportieren.

## Urheber/Entwickler

Nick Westphal

## Lizenz

Dieses Projekt wird unter der Open-Source-Hardware-Lizenz [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/cern_ohl_p_v2.txt) bereitgestellt. Für Details siehe [LICENSE](LICENSE).
